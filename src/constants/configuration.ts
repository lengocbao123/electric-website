export const COMPANY_NAME = "CÔNG TY TNHH THƯƠNG MẠI VÀ XÂY LẮP TRUNG NAM";
export const PHONE = "08 6265 8657";
export const EMAIL = "contact@gmail.com";
export const LOCATION = "489/9A Mã Lò, Phường Bình Hưng Hòa A, Quận Bình Tân, Thành phố Hồ Chí Minh, Việt Nam";
