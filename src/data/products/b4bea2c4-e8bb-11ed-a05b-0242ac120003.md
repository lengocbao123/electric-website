---
    id: "b4bea2c4-e8bb-11ed-a05b-0242ac120003"
    name: "TỦ ĐIỀU KHIỂN TRUNG TÂM"
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"
    price: 1249
    thumbnail: "https://ik.imagekit.io/gsozk5bngn/electric-web-app/products/thang-cap-400x100-son-tinh-dien-1-1377_g8-PPbTHw.jpg"
    images: [https://ik.imagekit.io/gsozk5bngn/electric-web-app/products/thang-cap-400x100-son-tinh-dien-1-1377_g8-PPbTHw.jpg"]
---
 Xây Dựng Điện Quang Nguyên cung cấp các dịch vụ thi công lắp đặt trọn gói trạm biến áp , thi công đường dây hạ thế, thi công đường cáp ngầm, đường dây trung thế lưới điện áp đến 35KV. Thi công các loại hình kết cấu trạm biến áp với công suất  từ 50KVA đến 5000KVA.

Các loại kết cấu trạm mà chúng tôi thi công lắp đặt như:

*   Thi công lắp đặt trạm biến áp treo 
    
*   Thi công lắp đặt trạm biến áp Kios 
    
*   Thi công lắp đặt trạm biến áp nền 
    
*   Thi công lắp đặt trạm biến áp xây kín 
    
*   Thi công lắp đặt trạm biến áp kiểu trụ thép 
    

Ngoài ra chúng tôi còn có các dịch vụ kỹ thuật khác liên quan tới trạm biến áp như:

*   Sửa chữa, bảo dưỡng trạm biến áp 
    
*   Nâng công suất trạm biến áp, thay máy biến áp 
    
*   Di dời trạm biến áp, di dời cột điện 
    
*   Thí nghiệm định kỳ trạm biến áp, thiết bị điện 
    

     Với chất lượng công trình luôn được chúng tôi đặc biệt chú trọng đến, mà từ đó luôn đảm bảo cho công trình được an toàn, và bền đẹp. Các dự án của chúng tôi sau khi hoàn thành đuợc đối tác Việt Nam, Hàn Quốc, Nhật Bản, Trung Quốc đánh giá cao về uy tín và sự chuyên nghiệp, được tư vấn giám sát hài lòng.