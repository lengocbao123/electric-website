---
  id: "16336440-e8b7-11ed-a05b-0242ac120003"
  title: "Vì sao cần bổ sung công suất thủy điện tích năng vào Quy hoạch điện VIII?"
  description: "heo nhìn nhận của nhiều chuyên gia, nhà quản lý, thì dù Việt Nam có các nguồn điện linh hoạt bổ sung cho hệ thống điện thì việc cắt giảm nguồn năng lượng gió, mặt trời với một tỷ lệ thích hợp là điều không thể tránh khỏi đối với một hệ thống điện tích hợp năng lượng tái tạo ở quy mô lớn... Vậy giải pháp nào có thể giải quyết vấn đề này? Chuyên gia Tạp chí Năng lượng Việt Nam cho rằng: Với tiềm năng phát triển thủy điện tích năng ở nước ta có thể đạt tới 12.500 MW, vì vậy, Chính phủ cần xem xét bổ sung công suất từ nguồn điện này cao hơn 1.200 MW vào năm 2030 và 7.800 MW vào năm 2045."
  postedDate: "2012-04-23T18:25:43.511Z"
  thumbnail: "https://ik.imagekit.io/gsozk5bngn/electric-web-app/blogs/fre-sonneveld-q6n8nIrDQHE-unsplash__1__4njCdJ6wF.jpg"
---

Bảo đảm dự thầu là một trong những điều kiện tiên quyết trước khi bên mời thầu tiến hành đánh giá hồ sơ dự thầu của nhà thầu. Rất nhiều nhà thầu trượt thầu chỉ vì sai sót trong bảo đảm dự thầu, kể cả những nhà thầu dày dạn kinh nghiệm. Nguyên nhân thì có rất nhiều do trước khi phát hành bản chính thức, bản draft được gửi qua lại giữa nhà thầu và ngân hàng, qua nhiều tầng kiểm tra nhưng chỉ cần 1 khâu sai sót, nhầm lẫn là có thể dẫn tới bảo đảm dự thầu không hợp lệ. Dưới đây là những lỗi mà nhà thầu thường gặp phải.

> Điều 4 của Luật Đấu thầu số 43/2013/NĐ-CP: “Bảo đảm dự thầu là việc nhà thầu, nhà đầu tư thực hiện một trong các biện pháp đặt cọc, ký quỹ hoặc nộp thư bảo lãnh của tổ chức tín dụng hoặc chi nhánh ngân hàng nước ngoài được thành lập theo pháp luật Việt Nam để đảm bảo trách nhiệm dự thầu của nhà thầu, nhà đầu tư trong thời gian xác định theo yêu cầu của hồ sơ mời thầu, hồ sơ yêu cầu.”

## Sai tên gói thầu, tên dự án hoặc tên dự toán mua sắm trong thư bảo lãnh dự thầu

Bạn tham dự gói thầu có tên là A nhưng bảo đảm dự thầu lại cấp cho gói thầu có tên B thì sẽ gây khó khăn cho bên mời thầu trong việc thu bảo đảm dự thầu của nhà thầu trong trường hợp nhà thầu không tuân thủ một trong các quy định nêu trong bảo đảm dự thầu. Để tránh sai sót, bạn hãy copy tên gói thầu, tên dự án hoặc dự toán mua sắm vào trong bảo đảm dự thầu và đặt nó trong dấu ngoặc kép. Bạn có thể bôi đậm để thuận tiện cho việc kiểm tra các trường thông tin này.

## Giá trị bảo đảm dự thầu nhỏ hơn giá trị yêu cầu

Nghe thì có vẻ vô lý nhưng đây cũng là lỗi mà nhiều nhà thầu hoặc ngân hàng mắc phải. Giá trị bảo đảm dự thầu thường được nêu ngay trong thông báo mời thầu và trong bảng dữ liệu. Tuy nhiên, nhiều ngân hàng lại căn cứ tỉ lệ % bảo đảm dự thầu so với giá gói thầu theo hướng dẫn dành cho bên mời thầu. Bên mời thầu có thể làm tròn con số này lên một chút mà ngân hàng lại cứng nhắc thì sẽ dẫn đến thiếu sót một vài trăm thậm chí một vài đồng. Bạn cần lưu ý rằng, dù chỉ thiếu một đồng thì bảo đảm dự thầu của bạn vẫn không hợp lệ và hồ sơ của bạn bị loại ngay lập tức.

## Thư bảo lãnh dự thầu sai thông tin đơn vị thụ hưởng

Trường hợp này xảy ra khi bên mời thầu và chủ đầu tư là hai tên khác nhau và địa chỉ khác nhau. Nếu làm ẩu, bạn có thể copy nhầm địa chỉ của đơn vị thụ hưởng. Hãy tìm đến đúng địa chỉ của Bên mời thầu để copy thông tin địa chỉ đơn vị thụ hưởng.

## Sai thời gian có hiệu lực của bảo đảm dự thầu

Để tránh sai sót, bạn hãy tìm đến E-CDNT 17.1 - Bảng dữ liệu để xem thông tin thời gian hiệu lực của bảo đảm dự thầu (bao nhiêu ngày?) và ngày đóng thầu trong thông báo mời thầu.

Trường hợp phía ngân hàng muốn ghi cụ thể hơn nữa ví dụ:

“Bảo đảm dự thầu có hiệu lực 120 ngày kể từ ngày (ngày đóng thầu) đến 17h00 ngày (kết thúc 120 ngày)”

Lý do phía ngân hàng đưa ra là phía ngân hàng chỉ giao dịch đến 17h00 nên sau thời điểm đó bảo đảm dự thầu không còn ý nghĩa. Hãy đấu tranh bằng được để ngân hàng bỏ thời gian này đi nếu không bảo đảm dự thầu của bạn cũng sẽ không hợp lệ. Ngoài ra, nếu ngân hàng cẩn thận ghi thêm ngày kết thúc thì bạn cần kiểm tra lại từ ngày đóng thầu đến ngày kết thúc có đủ 120 ngày hay không. Bạn đưa 2 ngày này vào 2 ô trong excel và làm phép trừ nhé.

## Thư bảo lãnh dự thầu không theo mẫu của bên mời thầu

Trong quá trình đánh giá HSDT, bên mời thầu cũng gặp nhiều khó khăn khi thu bảo đảm dự thầu của nhà thầu vì những lý do mà theo phía ngân hàng hoặc nhà thầu là chưa thuyết phục. Vì vậy bên mời thầu chủ động đưa ra mẫu riêng trong đó có thêm một vài điều khoản có lợi cho bên mời thầu trong việc tịch thu bảo đảm dự thầu của nhà thầu. Mẫu riêng này thường nằm trong file đính kèm trong thông báo mời thầu. Nếu bạn bỏ qua mẫu này và sử dụng mẫu chung cho các gói thầu thì bảo đảm dự thầu của bạn cũng sẽ không hợp lệ. Một ví dụ nếu bạn tham gia các gói thầu của bên Điện lực bạn sẽ thấy mẫu riêng của họ có thêm 6 từ: “vô điều kiện, không hủy ngang” mà các nhà thầu hay gọi vui là 6 chữ vàng.

Ngoài bảo đảm dự thầu, bạn cũng cần kiểm tra file đính kèm xem có biểu mẫu riêng nào không. Nếu có, bạn buộc phải tuân thủ mẫu của bên mời thầu đấy!

## Thư bảo lãnh dự thầu nêu thiếu điều kiện tịch thu bảo đảm dự thầu

Các trường hợp bên mời thầu được tịch thu bảo đảm dự thầu của nhà thầu được nêu trong mẫu bảo đảm dự thầu bao gồm:

1.  Nhà thầu rút E-HSDT sau thời điểm đóng thầu và trong thời gian có hiệu lực của E-HSDT;
2.  Nhà thầu vi phạm pháp luật về đấu thầu dẫn đến phải hủy thầu theo quy định;
3.  Nhà thầu không tiến hành hoặc từ chối tiến hành thương thảo hợp đồng trong thời hạn 5 ngày làm việc, kể từ ngày nhận được thông báo mời đến thương thảo hợp đồng của Bên mời thầu, trừ trường hợp bất khả kháng;
4.  Nhà thầu không tiến hành hoặc từ chối tiến hành hoàn thiện hợp đồng trong thời hạn 20 ngày, kể từ ngày nhận được thông báo trúng thầu của Bên mời thầu hoặc đã hoàn thiện hợp đồng nhưng từ chối ký hợp đồng, trừ trường hợp bất khả kháng;
5.  Nhà thầu không thực hiện biện pháp bảo đảm thực hiện hợp đồng theo quy định.

Nếu trong bảo đảm dự thầu của ngân hàng cấp thiếu một trong các điều kiện trên thì bảo đảm dự thầu được đánh giá là không hợp lệ.

## Trường hợp mất bản gốc thư bảo lãnh dự thầu 

Trường hợp nhà thầu được mời vào thương thảo hợp đồng tuy nhiên vì một lý do nào đó nhà thầu đánh mất bản gốc bảo đảm dự thầu? Tình huống này bên mời thầu xử lý như thế nào?

Đây là tình huống hiếm gặp nên khiến cho nhiều bên mời thầu bối rối. Do luật cũng chưa có quy định cụ thể về xử lý tình huống này nên có trường hợp Bên mời thầu đánh trượt nhà thầu và mời nhà thầu xếp hạng tiếp theo vào thương thảo. Tuy nhiên, một số bên mời thầu cũng chấp thuận khi phía ngân hàng cấp bảo đảm dự thầu xác nhận việc đã cấp bảo đảm dự thầu cho nhà thầu đồng thời cấp thay thế một bảo đảm dự thầu khác và đảm bảo đầy đủ trách nhiệm của của nhà thầu và quyền lợi của bên mời thầu.

Để tránh những rắc rối này, sau khi nộp thầu bạn cần lưu giữ bộ gốc HSDT cẩn thận kẻo có ngày bị trượt oan uổng nhé.

## Câu hỏi thường gặp về bảo đảm dự thầu

### Nộp bảo đảm dự thầu qua mạng bằng tiền mặt hoặc séc bảo chi của ngân hàng được không?

Đây là thông tin trích từ Điều 17 Chương I - Chỉ dẫn nhà thầu

> Khi tham dự thầu qua mạng, nhà thầu phải thực hiện biện pháp bảo đảm dự thầu trước thời điểm đóng thầu theo hình thức thư bảo lãnh do ngân hàng hoặc tổ chức tín dụng hoạt động hợp pháp tại Việt Nam phát hành

Như vậy khi tham gia dự thầu qua mạng, nhà thầu cần tuân thủ yêu cầu thực hiện biện pháp bảo đảm dự thầu theo hình thức thư bảo lãnh.

### Bảo đảm dự thầu có bắt buộc không?

> Điểm a Khoản 1 Điều 11 Luật Đấu thầu quy định bảo đảm dự thầu áp dụng trong các trường hợp đấu thầu rộng rãi, đấu thầu hạn chế, chào hàng cạnh tranh đối với gói thầu cung cấp dịch vụ phi tư vấn, mua sắm hàng hóa, xây lắp và gói thầu hỗn hợp.

Như vậy, chỉ có các gói thầu tư vấn thì nhà thầu mới không cần phải thực hiện biện pháp bảo đảm dự thầu.

### Bảo đảm dự thầu không được hoàn trả trong trường hợp nào?

Điều 17.3 Chương I Chỉ dẫn nhà thầu quy định:

Nhà thầu vi phạm quy định của pháp luật về đấu thầu dẫn đến không được hoàn trả giá trị bảo đảm dự thầu trong các trường hợp sau đây:

- Nhà thầu có văn bản rút E-HSDT sau thời điểm đóng thầu và trong thời gian có hiệu lực của E-HSDT;
- Nhà thầu vi phạm pháp luật về đấu thầu dẫn đến phải hủy thầu theo quy định tại điểm d Mục 30.1 E-CDNT;
- Nhà thầu không thực hiện biện pháp bảo đảm thực hiện hợp đồng theo quy định tại Mục 37 E-CDNT;
- Nhà thầu không tiến hành hoặc từ chối tiến hành thương thảo hợp đồng trong thời hạn 5 ngày làm việc, kể từ ngày nhận được thông báo mời đến thương thảo hợp đồng của Bên mời thầu, trừ trường hợp bất khả kháng;
- Nhà thầu không tiến hành hoặc từ chối tiến hành hoàn thiện, ký kết hợp đồng trong thời gian quy định tại Thông báo chấp thuận E-HSDT và trao hợp đồng của Bên mời thầu, trừ trường hợp bất khả kháng.

Khi vi phạm một trong các điều trên, nhà thầu buộc phải nộp bản gốc thư bảo lãnh dự thầu cho bên mời thầu. Nếu từ chối thực hiện, nhà thầu sẽ bị xử lý theo đúng cam kết của nhà thầu trong đơn dự thầu.

### Thực hiện bảo đảm dự thầu cho hồ sơ dự thầu liên danh như thế nào?

Nhà thầu có thể thực hiện bảo đảm dự thầu cho liên danh theo một trong 2 cách sau đây:

- Từng thành viên liên danh sẽ thực hiện riêng rẽ bảo đảm dự thầu cho phần công việc thực hiện của mình. Giá trị bảo đảm dự thầu phải tương ứng với giá trị phần công việc thực hiện so với tổng giá trị chào thầu.
- Thành viên đứng đầu liên danh sẽ thực hiện bảo đảm dự thầu cho cả liên danh.

Lưu ý khi thực hiện bảo đảm dự thầu liên danh:

- Tổng giá trị bảo đảm dự thầu không thấp hơn giá trị bảo đảm dự thầu quy định tại mục 17.1E-CDNT
- Nếu bảo đảm dự thầu của một thành viên được xác định là không hợp lệ thì E-HSDT của liên danh sẽ không được xem xét đánh giá tiếp.

Do vậy, thông thường thì nhà thầu chọn cách thứ 2 để giảm thiểu rủi ro khi làm bảo lãnh dự thầu.

Trường hợp làm theo cách 1, các thành viên liên danh còn phải tính toán tỷ lệ giá trị bảo đảm dự thầu cho phù hợp. Ví dụ minh họa là gói thầu có giá gói thầu là 1 tỷ. Giá trị bảo đảm dự thầu là 30 triệu đồng. Thành viên liên danh A thực hiện 40% giá trị gói thầu, thành viên liên danh B thực hiện 60% giá trị gói thầu. Giá trị bảo lãnh dự thầu của từng thành viên liên danh sẽ là:

- Thành viên liên danh A: 40% \* 30 triệu = 12 triệu
- Thành viên liên danh B: 60% \* 30 triệu = 18 triệu.

### Tỷ lệ bảo lãnh dự thầu so với giá gói thầu?

Giá trị bảo đảm dự thầu được quy định trong hồ sơ mời thầu, hồ sơ yêu cầu theo một mức xác định từ 1% - 3% giá gói thầu căn cứ quy mô và tính chất của từng gói thầu cụ thể. Thông thường là 3%. Bạn cũng có thể căn cứ vào giá trị bảo đảm dự thầu để tạm tính giá gói thầu nếu HSMT không thông báo giá trị gói thầu. Ngoài ra, bạn cũng có thể căn cứ vào giá trị hợp đồng tương tự (thông thường là 70%) để tính ra giá gói thầu.

\----------

Ghi chú: Đây là chia sẻ kinh nghiệm từ thực tế gặp phải của thành viên trong team bidwinner để bạn tham khảo. Vui lòng không chia sẻ, dẫn chiếu thông tin. Bidwinner không chịu trách nhiệm về tính chính xác trong các thông tin ở trên.

Nếu chưa biết cách lập hồ sơ dự thầu qua mạng, bạn có thể tham khảo các bước lập
