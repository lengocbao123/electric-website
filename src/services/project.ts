import PROJECTS from "@/data/project.json";
export const getProjectById = (id: string) => {
  return PROJECTS.find((project) => project.id === id);
};
