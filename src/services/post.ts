import fs from "fs";
import path, { join } from "path";
import matter from "gray-matter";
import { Blog } from "@/types";

const postsDirectory = path.join(process.cwd(), "src/data/posts");

export const getPosts = () => {
  // Get file names under /posts
  const fileNames = fs.readdirSync(postsDirectory);
  const allPostsData = fileNames.map((fileName) => {
    // Remove ".md" from file name to get id
    const id = fileName.replace(/\.md$/, "");

    // Read markdown file as string
    const fullPath = path.join(postsDirectory, fileName);
    const fileContents = fs.readFileSync(fullPath, "utf8");

    // Use gray-matter to parse the post metadata section
    const matterResult = matter(fileContents);

    // Combine the data with the id
    return {
      id,
      ...matterResult.data,
      content: "",
    } as Blog;
  });

  return allPostsData;
};
export const getPostById = (id: string) => {
  const fullPath = join(postsDirectory, `${id}.md`);
  const fileContents = fs.readFileSync(fullPath, "utf8");
  const { data, content } = matter(fileContents);

  return {
    id: data.id,
    title: data.title,
    description: data.description,
    postedDate: data.postedDate,
    thumbnail: data.thumbnail,
    content,
  } as Blog;
};
