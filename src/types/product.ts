export type Product = {
    id: string;
    name: string;
    description: string;
    detail: string;
    images: Array<string>;
    thumbnail: string;
  };