export type Blog = {
  id: string;
  title: string;
  description: string;
  content: string;
  postedDate: string;
  thumbnail: string;
};
