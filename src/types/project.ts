export type Project = {
  id: string;
  name: string;
  description: string;
  detail?: string;
  images: Array<string>;
  thumbnail: string;
  location: string;
  totalInvestment: string;
  wattage: number;
  startDate: string;
  status: string;
  investor: string;
};
