import HorizontalBlogCard from "@/components/blog/horizontal-blog-card";
import { Layout } from "@/components/layout";
import { NextPageWithLayout } from "@/pages/_app";
import { getPostById, getPosts } from "@/services";
import { Blog } from "@/types";
import markdownToHtml from "@/utils/markdownToHtml";
import { convertToSlug } from "@/utils/text";
import { CalendarIcon } from "@heroicons/react/24/outline";
import { format, parseISO } from "date-fns";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { NextSeo } from "next-seo";
import Image from "next/image";
import { Fragment } from "react";

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { id } = query;
  const blog: Blog = getPostById(id as string);
  const content = await markdownToHtml(blog.content);
  const relatedBlogs: Array<Blog> = await getPosts();

  return {
    props: { blog: { ...blog, content }, relatedBlogs },
  };
};
const BlogDetailPage: NextPageWithLayout = ({
  blog,
  relatedBlogs,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Fragment>
      <NextSeo title={blog.title} description={blog.description} />

      <div className="container mx-auto py-7.5 grid grid-cols-1 sm:grid-cols-3 gap-5">
        <div className="flex flex-col sm:col-span-2 gap-4">
          <h1 className="text-xl font-bold">{blog.title}</h1>
          <div className="flex items-center gap-2">
            <CalendarIcon className="h-4 w-4 text-neutral-500" />
            <span className="text-sm text-neutral-500">
              {format(parseISO(blog.postedDate), "dd/MM/yyy")}
            </span>
          </div>
          <Image
            alt={blog.title}
            src={blog.thumbnail}
            width={792}
            height={446}
            className="aspect-[792/446] w-full bg-neutral-400 object-cover object-center"
          />
          <div
            className="sm:col-span-2 text-sm text-neutral-800 text-justify flex flex-col gap-3"
            dangerouslySetInnerHTML={{ __html: blog.content }}
          ></div>
        </div>
        <div className="flex flex-col rounded-sm border h-fit">
          <h2 className="px-2 py-4 text-sm font-bold bg-neutral-300">
            TIN TỨC LIÊN QUAN
          </h2>
          <div className="flex flex-col items-start gap-5 p-4">
            {relatedBlogs.slice(0, 3).map((blog: Blog) => (
              <HorizontalBlogCard
                url={`/tin-tuc/${convertToSlug(blog.title)}/${blog.id}`}
                key={blog.id}
                {...blog}
              />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
};
BlogDetailPage.getLayout = (page) => <Layout>{page}</Layout>;

export default BlogDetailPage;
