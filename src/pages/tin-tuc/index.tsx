import { Layout } from "@/components/layout";
import { NextPageWithLayout } from "@/pages/_app";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { Fragment } from "react";
import HorizontalBlogCard from "@/components/blog/horizontal-blog-card";
import { convertToSlug } from "@/utils/text";
import BlogCard from "@/components/blog/blog-card";
import { Pagination } from "@/components/ui/pagination/pagination";
import { Blog } from "@/types";
import { NextSeo } from "next-seo";
import { getPosts } from "@/services";

export const getServerSideProps: GetServerSideProps = async () => {
  const blogs: Array<Blog> = await getPosts();

  return {
    props: { blogs },
  };
};

const BlogsPage: NextPageWithLayout = ({
  blogs,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const bigBlog = blogs[0];
  const featuredBlogs = blogs.slice(1, 4);

  return (
    <Fragment>
      <NextSeo title="Tin Tức" />
      <div className="container mx-auto py-7.5 flex  flex-col gap-7.5 px-4 sm:px-0">
        <div className="flex flex-col items-center">
          <h1 className="text-primary-600 font-bold text-2xl whitespace-nowrap uppercase">
            Tin Tức
          </h1>
          <div className="w-10 h-1 bg-primary-600 mb-7.5"></div>
        </div>
        <div className="flex flex-col divide-y px-4 sm:px-0">
          <div className="text-base font-bold border-b-2 border-neutral-800 w-fit">
            NỔI BẬT
          </div>
          <div className="w-full grid grid-cols-1 sm:grid-cols-3 gap-5 py-7.5">
            <BlogCard
              url={`/tin-tuc/${convertToSlug(bigBlog.title)}/${bigBlog.id}`}
              {...bigBlog}
            />
            <div className="sm:col-span-2 flex flex-col gap-5">
              {featuredBlogs.map((blog: Blog) => (
                <HorizontalBlogCard
                  url={`/tin-tuc/${convertToSlug(blog.title)}/${blog.id}`}
                  key={blog.id}
                  {...blog}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="flex flex-col divide-y px-4 sm:px-0">
          <div className="text-base font-bold border-b-2 border-neutral-800 w-fit">
            CÁC TIN KHÁC
          </div>
          <div className="flex flex-col items-center gap-7.5 py-7.5">
            {blogs.map((blog: Blog) => (
              <HorizontalBlogCard
                url={`/tin-tuc/${convertToSlug(blog.title)}/${blog.id}`}
                key={blog.id}
                {...blog}
              />
            ))}
            <Pagination totalPages={1} page={1} />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

BlogsPage.getLayout = (page) => <Layout>{page}</Layout>;

export default BlogsPage;
