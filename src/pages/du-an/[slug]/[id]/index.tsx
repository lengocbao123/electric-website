import { Layout } from "@/components/layout";
import ProjectDetail from "@/components/project/project-detail";
import ProjectInformation from "@/components/project/project-info";
import { NextPageWithLayout } from "@/pages/_app";
import { getProjectById } from "@/services";
import { InferGetServerSidePropsType } from "next";
import { NextSeo } from "next-seo";
import { Fragment } from "react";

export async function getServerSideProps({ query }: { query: any }) {
  const { id } = query;
  const project = getProjectById(id as string);
  if (!project) {
    return {
      props: {},
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: { project },
  };
}
const ProjectDetailPage: NextPageWithLayout = ({
  project,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Fragment>
      <NextSeo title={project?.name} description={project?.description} />
      <div className="container mx-auto py-7.5 flex items-center flex-col gap-7.5">
        {project && <ProjectInformation project={project} />}
        {project && <ProjectDetail project={project} />}
      </div>
    </Fragment>
  );
};
ProjectDetailPage.getLayout = (page) => <Layout>{page}</Layout>;

export default ProjectDetailPage;
