import { Layout } from "@/components/layout";
import { NextSeo } from "next-seo";
import ProjectList from "@/components/project/project-list";
import { Pagination } from "@/components/ui/pagination/pagination";
import PROJECTS from "@/data/project.json";

export default function ProductPage() {
  return (
    <Layout>
      <NextSeo title="DỰ ÁN" />
      <div className="container mx-auto py-7.5 flex items-center flex-col px-4 sm:px-0">
        <h1 className="text-center text-primary-600 font-bold text-2xl whitespace-nowrap">
          DỰ ÁN
        </h1>
        <div className="w-10 h-1 bg-primary-600 mb-7.5"></div>
        <ProjectList projects={PROJECTS} />
        <Pagination totalPages={1} page={1} />
      </div>
    </Layout>
  );
}
