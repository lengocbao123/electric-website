import { InferGetServerSidePropsType } from "next";
import { NextSeo } from "next-seo";
import { Fragment } from "react";
import { getServerSideProps } from ".";
import { NextPageWithLayout } from "@/pages/_app";
import Link from "next/link";
import {
  ArrowRightIcon,
  ExclamationTriangleIcon,
} from "@heroicons/react/24/outline";

const Error404Page: NextPageWithLayout = ({}: InferGetServerSidePropsType<
  typeof getServerSideProps
>) => {
  return (
    <Fragment>
      <NextSeo title="Không tìm thấy trang" />
      <div className="h-full flex flex-col gap-3 items-center justify-center mt-20">
        <ExclamationTriangleIcon className="text-error-400 h-10 w-10" />
        <h1 className="text-2xl font-bold ">404 - Không tìm thấy trang</h1>
        <Link
          aria-label="Quay lại trang chủ"
          title="Quay lại trang chủ"
          className="text-primary-600 flex items-center gap-2"
          href="/"
        >
          Quay lại trang chủ <ArrowRightIcon className="w-6 h-6" />
        </Link>
      </div>
    </Fragment>
  );
};

export default Error404Page;
