import ContactForm from "@/components/contact/contact-form";
import { Layout } from "@/components/layout";
import { EMAIL, LOCATION, PHONE } from "@/constants/configuration";
import {
  EnvelopeIcon,
  MapPinIcon,
  PhoneIcon,
} from "@heroicons/react/24/outline";
import { NextSeo } from "next-seo";

export default function ContactPage() {
  return (
    <Layout>
      <NextSeo title="Liên lạc" />
      <div className="container mx-auto py-7.5 flex items-center flex-col">
        <h1 className="text-center text-primary-600 font-bold text-2xl whitespace-nowrap">
          Liên lạc
        </h1>
        <div className="w-10 h-1 bg-primary-600 mb-7.5"></div>
        <div className="container mx-auto px-4 sm:px-4 grid grid-cols-1 sm:grid-cols-2 gap-10">
          <div className="flex flex-col gap-5">
            <h3 className="text-xl font-bold">Gọi ngay hoặc gửi phản hồi!</h3>
            <p className="text-sm text-neutral-600">
              Hãy gửi phản hồi của bạn thông qua số điện thoại hoặc form liên hệ
              dưới đây, chúng tôi sẽ phản hồi cho bạn trong thời gian sớm nhất.
            </p>
            <div className="flex flex-col divide-y">
              <div className="flex items-center gap-2 py-4">
                <MapPinIcon className="text-neutral-600 h-4 w-4" />
                <div className="text-sm text-neutral-600">{LOCATION}</div>
              </div>
              <div className="flex items-center gap-2 py-4">
                <PhoneIcon className="text-neutral-600 h-4 w-4" />
                <div className="text-sm text-neutral-600">{PHONE}</div>
              </div>
              <div className="flex items-center gap-2 py-4">
                <EnvelopeIcon className="text-neutral-600 h-4 w-4" />
                <div className="text-sm text-neutral-600">{EMAIL}</div>
              </div>
            </div>
          </div>
          <ContactForm />
        </div>
      </div>
    </Layout>
  );
}
