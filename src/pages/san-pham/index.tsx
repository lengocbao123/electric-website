import { Layout } from "@/components/layout";
import ProductList from "@/components/product/product-list";
import { Pagination } from "@/components/ui/pagination/pagination";
import { getProducts } from "@/services";
import { Product } from "@/types";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { NextSeo } from "next-seo";
import { NextPageWithLayout } from "@/pages/_app";
import { Fragment } from "react";

export const getServerSideProps: GetServerSideProps = async () => {
  const products: Array<Product> = await getProducts();

  return {
    props: { products },
  };
};
const ProductsPage: NextPageWithLayout = ({
  products,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Fragment>
      <NextSeo title="SẢN PHẨM" />
      <div className="container mx-auto py-7.5 flex items-center flex-col px-4 sm:px-0">
        <h1 className="text-center text-primary-600 font-bold text-2xl whitespace-nowrap">
          SẢN PHẨM
        </h1>
        <div className="w-10 h-1 bg-primary-600 mb-7.5"></div>
        <ProductList products={products} />
        <Pagination totalPages={1} page={1} />
      </div>
    </Fragment>
  );
};
ProductsPage.getLayout = (page) => <Layout>{page}</Layout>;

export default ProductsPage;
