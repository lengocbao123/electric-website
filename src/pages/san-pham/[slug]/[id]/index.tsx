import { Layout } from "@/components/layout";
import ProductDetail from "@/components/product/product-detail";
import ProductInformation from "@/components/product/product-information";
import Error404Page from "@/pages/404";
import { NextPageWithLayout } from "@/pages/_app";
import { getProductById } from "@/services";
import markdownToHtml from "@/utils/markdownToHtml";
import { convertToSlug } from "@/utils/text";
import { InferGetServerSidePropsType } from "next";
import { NextSeo } from "next-seo";
import { Fragment } from "react";

export async function getServerSideProps({ query }: { query: any }) {
  const { id } = query;
  const product = getProductById(id as string);
  if (!product) {
    return {
      props: {},
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }
  const detail = await markdownToHtml(product.detail);

  return {
    props: { product: { ...product, detail } },
  };
}
const ProductDetailPage: NextPageWithLayout = ({
  product,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  if (!product) {
    return <Error404Page />;
  }

  return (
    <Fragment>
      <NextSeo
        title={product?.name}
        description={product?.description}
        canonical={`/san-pham/${convertToSlug(product?.name)}/${product.id}`}
      />
      <div className="container mx-auto py-7.5 flex items-center flex-col gap-7.5">
        {product && <ProductInformation product={product} />}
        {product && <ProductDetail product={product} />}
      </div>
    </Fragment>
  );
};
ProductDetailPage.getLayout = (page) => <Layout>{page}</Layout>;

export default ProductDetailPage;
