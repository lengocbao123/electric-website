import Slides from "@/components/ui/slide";
import ServiceSection from "@/components/service/service-section";
import ProductSection from "@/components/product/product-section";
import BlogSection from "@/components/blog/blog-section";
import ContactSection from "@/components/contact/contact-section";
import { Layout } from "@/components/layout";
import { NextSeo } from "next-seo";
import { COMPANY_NAME } from "@/constants/configuration";
import { Blog, Product } from "@/types";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { getPosts, getProducts } from "@/services";
import { NextPageWithLayout } from "@/pages/_app";
import { Fragment } from "react";

export const getServerSideProps: GetServerSideProps = async () => {
  const blogs: Array<Blog> = await getPosts();
  const products: Array<Product> = await getProducts();
  return {
    props: { blogs, products },
  };
};

const HomePage: NextPageWithLayout = ({
  blogs,
  products,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Fragment>
      <NextSeo title={COMPANY_NAME} />
      <div className="flex flex-col">
        <Slides />
        <ContactSection />
        <div className="container mx-auto px-4 flex flex-col gap-20 mt-20">
          <ServiceSection />
          <ProductSection products={products} />
          <BlogSection blogs={blogs} />
        </div>
      </div>
    </Fragment>
  );
};
HomePage.getLayout = (page) => <Layout>{page}</Layout>;

export default HomePage;
