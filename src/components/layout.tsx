import Footer from "@/components/ui/footer";
import HeaderMenu, { MenuItem } from "@/components/menu/header-menu";
import MobileHeaderMenu from "@/components/menu/mobile-header-menu";
import { FC, Fragment, HTMLAttributes } from "react";
import { Inter } from "next/font/google";
import { PhoneIcon } from "@heroicons/react/24/outline";
import { PHONE } from "@/constants/configuration";

export type LayoutProps = HTMLAttributes<HTMLElement>;

export const BANNERS = [
  {
    id: "1",
    header: "Let&apos;s start with your company information.",
    description:
      "First things first, please provide the information about your company.",
    image: "/images/banner-1.png",
  },
  {
    id: "2",
    header: "Tell us a bit more about you",
    description:
      "First things first, please provide the information about your company.",
    image: "/images/banner-2.png",
  },
  {
    id: "3",
    header: "Tell us a bit more about you",
    description:
      "First things first, please provide the information about your company.",
    image: "/images/banner-3.png",
  },
];
const inter = Inter({ subsets: ["latin"] });
const MENU: Array<MenuItem> = [
  {
    id: "trang-chu",
    text: "Trang chủ",
    href: "/",
  },
  {
    id: "san-pham",
    text: "Sản phẩm",
    href: "/san-pham",
  },
  {
    id: "du-an",
    text: "Dự án",
    href: "/du-an",
  },
  {
    id: "tin-tuc",
    text: "Tin tức",
    href: "/tin-tuc",
  },
  {
    id: "lien-he",
    text: "Hỗ trợ & Liên hệ",
    href: "/lien-he",
  },
];
export const Layout: FC<LayoutProps> = (props) => {
  const { children } = props;

  return (
    <Fragment>
      <HeaderMenu items={MENU} />
      <MobileHeaderMenu items={MENU} />
      <main className={`flex min-h-screen flex-col ${inter.className}`}>
        {children}
        <a title="Số điện thoại" href={`callto:${PHONE}`} className="fixed bottom-4 left-4 flex bg-primary-600 h-15 w-15 rounded-full items-center justify-center">
          <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-primary-600 opacity-75"></span>
          <span className="relative flex rounded-full bg-primary-600 h-15 w-15 items-center justify-center">
            <PhoneIcon className="h-6 w-6 text-neutral" />
          </span>
        </a>
      </main>
      <Footer />
    </Fragment>
  );
};
