import { CalendarIcon } from "@heroicons/react/24/outline";
import { format, parseISO } from "date-fns";
import Image from "next/image";
import Link from "next/link";
import type { FC } from "react";

interface HorizontalBlogCardProps {
  title: string;
  postedDate: string;
  url: string;
  thumbnail: string;
  description?: string;
}

const HorizontalBlogCard: FC<HorizontalBlogCardProps> = ({
  title,
  description,
  postedDate,
  url,
  thumbnail,
}) => {
  return (
    <div className="w-full flex gap-2 p-4 border rounded-sm">
      <Image
        alt={title}
        src={thumbnail}
        height={250}
        width={380}
        className="object-cover max-w-[150px] h-auto"
      />
      <div className="flex-grow flex-col gap-5">
        <div className="flex items-center gap-2">
          <CalendarIcon className="h-4 w-4 text-neutral-500" />
          <span className="text-sm text-neutral-500">
            {format(parseISO(postedDate), "dd/MM/yyy")}
          </span>
        </div>
        <Link
          aria-label={title}
          title={title}
          className="text-base text-primary-600 font-bold"
          href={url}
        >
          {title}
        </Link>
        <p className="text-sm text-neutral-600 line-clamp-3">{description}</p>
      </div>
    </div>
  );
};
export default HorizontalBlogCard;
