import { CalendarIcon } from "@heroicons/react/24/outline";
import { format, parseISO } from "date-fns";
import Image from "next/image";
import Link from "next/link";
import type { FC } from "react";

interface BlogCardProps {
  title: string;
  postedDate: string;
  url: string;
  thumbnail: string;
  description?: string;
}

const BlogCard: FC<BlogCardProps> = ({
  title,
  description,
  postedDate,
  url,
  thumbnail,
}) => {
  return (
    <div className="flex flex-col gap-2 p-4 border rounded-sm">
      <div className="relative">
        <Image
          alt={title}
          src={thumbnail}
          height={165}
          width={220}
          className="object-cover h-[165px] w-full"
        />
      </div>
      <Link aria-label={title} title={title} href={url}>
        <h3 className="text-base text-primary-600 font-bold line-clamp-2">{title}</h3>
      </Link>
      <p className="text-sm text-neutral-600 line-clamp-3">{description}</p>
      <div className="flex items-center gap-2">
        <CalendarIcon className="h-4 w-4 text-neutral-500" />
        <span className="text-sm text-neutral-500">
          {format(parseISO(postedDate), "dd/MM/yyy")}
        </span>
      </div>
    </div>
  );
};
export default BlogCard;
