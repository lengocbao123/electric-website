import { type FC } from "react";
import BlogCard from "@/components/blog/blog-card";
import { Blog } from "@/types";
import { convertToSlug } from "@/utils/text";
import SwiperSlides from "@/components/ui/swiper";

interface BlogSectionProps {
  blogs: Array<Blog>;
}

const BlogSection: FC<BlogSectionProps> = ({ blogs }) => {
  return (
    <div className="flex flex-col gap-5">
      <h1 className="text-2xl text-primary-600 font-bold">TIN TỨC NỔI BẬT</h1>
      <div>
        <SwiperSlides
          spaceBetween={30}
          slidesPerView={4}
          slides={blogs.map((blog) => {
            return (
              <BlogCard
                key={blog.id}
                url={`/tin-tuc/${convertToSlug(blog.title)}/${blog.id}`}
                {...blog}
              />
            );
          })}
        />
      </div>
    </div>
  );
};
export default BlogSection;
