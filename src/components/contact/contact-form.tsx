import type { FC } from "react";

interface ContactFormProps {}

const ContactForm: FC<ContactFormProps> = ({}) => {
  return (
    <form className="w-full grid grid-cols-1 sm:grid-cols-2 gap-5">
      <div className="flex flex-col">
        <label>Tên của bạn</label>
        <input
          className="w-full bg-transparent px-2 py-3 border rounded"
          name="name"
        />
      </div>
      <div className="flex flex-col">
        <label>Email</label>
        <input
          className="w-full bg-transparent px-2 py-3 border rounded"
          name="email"
        />
      </div>
      <div className="flex flex-col">
        <label>Số điện thoại</label>
        <input
          className="w-full bg-transparent px-2 py-3 border rounded"
          name="phone"
        />
      </div>
      <div className="flex flex-col">
        <label>Tiêu đề</label>
        <input
          className="w-full bg-transparent px-2 py-3 border rounded"
          name="title"
        />
      </div>
      <div className="sm:col-span-2 flex flex-col">
        <label>Nội dung</label>
        <textarea
          className="w-full bg-transparent px-2 py-3 border rounded"
          name="content"
        />
      </div>
      <button aria-label="send" className="bg-primary-600 px-2 py-3 rounded text-neutral">Gửi</button>
    </form>
  );
};
export default ContactForm;
