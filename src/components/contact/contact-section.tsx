import { EMAIL, PHONE } from "@/constants/configuration";
import { PencilSquareIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import type { FC } from "react";

const ContactSection: FC = () => {
  return (
    <div className="bg-neutral-100">
      <div className="flex flex-wrap container mx-auto px-4 sm:px-0 py-5 gap-4">
        <div className="flex-grow flex flex-wrap items-center gap-4 sm:gap-8">
          <div className="flex gap-2 items-center">
            <span className="text-sm font-bold">Hotline</span>
            <a
              title="Hotline"
              className="text-sm font-bold text-primary-600"
              href={`callto:${PHONE}`}
            >
              {PHONE}
            </a>
          </div>
          <span className="text-sm font-bold hidden sm:block">/</span>
          <div className="flex gap-2 items-center">
            <span className="text-sm font-bold">Tư vấn & Hỗ trợ</span>
            <a
              title="Tư vấn & Hỗ trợ"
              className="text-sm font-bold text-primary-600"
              href={`mailto:${EMAIL}`}
            >
              {EMAIL}
            </a>
          </div>
        </div>
        <Link
          title="Gửi yêu cầu"
          href="/lien-he"
          className="bg-primary-600 rounded p-4 flex items-center font-bold gap-2 text-sm text-white"
        >
          <PencilSquareIcon className="w-4 h-4" />
          Gửi yêu cầu
        </Link>
      </div>
    </div>
  );
};
export default ContactSection;
