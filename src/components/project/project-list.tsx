import { type FC } from "react";
import ProjectCard from "@/components/project/project-card";
import { Project } from "@/types";
import { convertToSlug } from "@/utils/text";

interface ProjectListProps {
  projects: Array<Project>;
}

const ProjectList: FC<ProjectListProps> = ({ projects }) => {
  return (
    <div className="flex flex-col gap-5">
      <div className="grid grid-cols-1 sm:grid-cols-4 gap-5">
        {projects.map((project) => (
          <ProjectCard
            key={project.id}
            name={project.name}
            image={project.thumbnail}
            description={project.description}
            url={`/du-an/${convertToSlug(project.name)}/${project.id}`}
          />
        ))}
      </div>
    </div>
  );
};
export default ProjectList;
