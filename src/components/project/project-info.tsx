import { Project } from "@/types";
import type { FC } from "react";
import Image from "next/image";
import { format, parseJSON } from "date-fns";

interface ProjectInfoProps {
  project: Project;
}

const ProjectInfo: FC<ProjectInfoProps> = ({ project }) => {
  return (
    <div className="w-full grid grid-cols-1 sm:grid-cols-3 gap-5 px-4 sm:px-0">
      <Image
        alt={project.name}
        src={project.images[0]}
        className="object-cover w-full h-auto"
        width={400}
        height={300}
      />
      <div className="col-span-2 flex flex-col gap-5">
        <h1 className="text-lg font-bold text-neutral-800">{project.name}</h1>
        <ul className="divide-y">
          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold">Vị trí:</div>
            <div className="text-sm col-span-2">{project.location}</div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold">Tổng mức đầu tư:</div>
            <div className="text-sm col-span-2">{project.totalInvestment}</div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold flex-grow">Công suất:</div>
            <div className="text-sm col-span-2">{project.wattage}MW</div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold flex-grow">Khởi công:</div>
            <div className="text-sm col-span-2">
              {format(parseJSON(project.startDate), "MM/yyyy")}
            </div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold flex-grow">Tình trạng:</div>
            <div className="text-sm col-span-2">{project.status}</div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold flex-grow">Chủ đầu tư:</div>
            <div className="text-sm col-span-2">{project.investor}</div>
          </li>

          <li className="grid grid-cols-3 py-4">
            <div className="text-sm font-bold flex-grow">
              Công việc thực hiện:
            </div>
            <div className="text-sm col-span-2">{project.description}</div>
          </li>
        </ul>
      </div>
    </div>
  );
};
export default ProjectInfo;
