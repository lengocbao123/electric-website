import { Project } from "@/types";
import type { FC } from "react";

interface ProjectDetailProps {
  project: Project;
}

const ProjectDetail: FC<ProjectDetailProps> = ({ project }) => {
  return (
    <div className="flex flex-col divide-y px-4 sm:px-0">
      <div className="text-base font-bold border-b-2 border-neutral-800 w-fit">
        Chi tiết
      </div>
      <div className="text-sm flex flex-col gap-3 py-5 ">
        <p>
          Nhà máy Điện gió Tân Thuận sẽ được lắp đặt 18 tua bin gió với công
          suất mỗi tua bin 4.15MW, sử dụng công nghệ hiện đại với thiết bị có
          xuất xứ Châu Âu, đảm bảo chất lượng và hiệu quả hoạt động lâu dài.
        </p>
        <p>
          Đối với dự án này, PECC2POM sẽ áp dụng công nghệ vận hành từ xa thông
          qua trung tâm điều khiển từ xa (PECC2-OCC) nhằm tối ưu hóa công tác
          vận hành và tiết kiệm chi phí.
        </p>
      </div>
    </div>
  );
};
export default ProjectDetail;
