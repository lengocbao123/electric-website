"use client";
import Image from "next/image";
import { FC } from "react";
import SwiperSlides from "@/components/ui/swiper";

interface SlidesProps {}

const Slides: FC<SlidesProps> = ({}) => {
  const BANNERS = [
    {
      id: "2",
      header: "Tell us a bit more about you",
      description:
        "First things first, please provide the information about your company.",
      image: "/images/banner-2.png",
    },
    {
      id: "1",
      header: "Let&apos;s start with your company information.",
      description:
        "First things first, please provide the information about your company.",
      image: "/images/banner-1.png",
    },
    {
      id: "3",
      header: "Tell us a bit more about you",
      description:
        "First things first, please provide the information about your company.",
      image: "/images/banner-3.png",
    },
    {
      id: "4",
      header: "Let&apos;s start with your company information.",
      description:
        "First things first, please provide the information about your company.",
      image: "/images/banner-1.png",
    },
  ];
  return (
    <div>
      <SwiperSlides
        autoplay
        slides={BANNERS.map((item) => {
          return (
            <Image
              key={item.id}
              src={item.image}
              alt={item.header}
              width={3000}
              height={100}
            />
          );
        })}
      />
    </div>
  );
};
export default Slides;
