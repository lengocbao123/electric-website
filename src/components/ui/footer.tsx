import {
  COMPANY_NAME,
  EMAIL,
  LOCATION,
  PHONE,
} from "@/constants/configuration";
import {
  EnvelopeIcon,
  MapPinIcon,
  PhoneIcon,
} from "@heroicons/react/24/outline";
import type { FC } from "react";

interface FooterProps {}

const Footer: FC<FooterProps> = ({}) => {
  return (
    <footer className="flex flex-col mt-20 bg-neutral-200 px-4 sm:px-0">
      <div className="container mx-auto grid grid-cols-1 sm:grid-cols-3 gap-5 py-7.5">
        <div className="sm:col-span-4 text-lg font-bold text-primary-600">
          {COMPANY_NAME}
        </div>
        <div className="flex items-center gap-2">
          <div className="p-2 bg-primary-600 rounded-sm">
            <MapPinIcon className="text-white h-4 w-4" />
          </div>
          <div className="text-sm text-neutral-600">{LOCATION}</div>
        </div>
        <div className="flex items-center gap-2">
          <div className="p-2 bg-primary-600 rounded-sm">
            <PhoneIcon className="text-white h-4 w-4" />
          </div>
          <div className="text-sm text-neutral-600">{PHONE}</div>
        </div>

        <div className="flex items-center gap-2">
          <div className="p-2 bg-primary-600 rounded-sm">
            <EnvelopeIcon className="text-white h-4 w-4" />
          </div>
          <div className="text-sm text-neutral-600">{EMAIL}</div>
        </div>
      </div>
      <div className="text-center text-sm text-white bg-primary-600 py-7.5">
        © 2017 Copyright by
        <span className="font-bold mx-1">{COMPANY_NAME}</span>
        Group. All rights reserved.
      </div>
    </footer>
  );
};
export default Footer;
