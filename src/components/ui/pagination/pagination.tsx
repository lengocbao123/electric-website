"use client";
import {
  ChevronLeftIcon,
  ChevronRightIcon,
  EllipsisHorizontalIcon,
} from "@heroicons/react/24/outline";
import { usePagination } from "@/hooks/use-pagination";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";
import { FC, HTMLAttributes } from "react";

export interface PaginationProps extends HTMLAttributes<HTMLUListElement> {
  onPaginationChange?: (page: number) => void;
  page: number;
  totalPages: number;

  /**
   * The variant to use.
   * @default 'text'
   */
  variant?: "text" | "icon";
}

export const Pagination: FC<PaginationProps> = ({
  className,
  page,
  totalPages,
  variant = "icon",
  onPaginationChange,
  ...rest
}) => {
  const router = useRouter();
  const { items } = usePagination({
    page,
    count: totalPages,
    disabled: totalPages === 1,
  });

  const Icons = {
    previous: <ChevronLeftIcon className="inline" />,
    next: <ChevronRightIcon className="inline" />,
  };

  return (
    <ul
      className={clsx("space-x-2 whitespace-nowrap py-4", className)}
      {...rest}
    >
      {items.map((item, index) => (
        <li key={index} className="inline-block align-middle">
          {["start-ellipsis", "end-ellipsis"].includes(item.type) ? (
            <EllipsisHorizontalIcon className="text-neutral-800" />
          ) : (
            <Link
              title={index.toString()}
              aria-label={item.page}
              className={clsx(
                "inline-flex min-w-[32px] items-center justify-center rounded-full font-medium",
                item.type === "page"
                  ? "py-1.75 px-1.75 text-xs"
                  : variant === "icon"
                  ? "py-1.75 px-1.75 min-w-[32px] border"
                  : "py-1.25 border px-4 text-sm capitalize",
                item.disabled
                  ? "pointer-events-none select-none opacity-50"
                  : "",
                item.selected
                  ? "text-neutral-0 bg-secondary border-secondary border"
                  : "hover:bg-secondary/20 hover:border-secondary/50 hover:text-neutral-500 text-neutral-800"
              )}
              href={{
                pathname: router.pathname,
                query: {
                  ...router.query,
                  page: item.page,
                },
              }}
              onClick={async (e) => {
                item.onClick(e);

                if (onPaginationChange) {
                  e.preventDefault();
                  await onPaginationChange(item.page);
                }
              }}
            >
              {item.type === "page"
                ? item.page
                : variant === "text"
                ? item.type
                : item.type === "previous"
                ? Icons.previous
                : Icons.next}
            </Link>
          )}
        </li>
      ))}
    </ul>
  );
};
