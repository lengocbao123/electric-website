import type { FC } from "react";

interface IntroProps {
  title: string;
  description: string;
}

const Intro: FC<IntroProps> = ({ title, description }) => {
  return (
    <div className="flex flex-col sm:flex-row  gap-7.5 divide-x-0 divide-y-2 sm:divide-y-0 sm:divide-x-2 divide-primary-600">
      <h2 className="text-center text-primary-600 font-bold text-2xl whitespace-nowrap">
        {title}
      </h2>
      <div className="text-justify text-sm flex-grow pl-0 pt-7.5 sm:pt-0 sm:pl-7.5">{description}</div>
    </div>
  );
};
export default Intro;
