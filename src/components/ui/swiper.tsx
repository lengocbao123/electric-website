"use client";
import type { FC } from "react";
import "swiper/css";
import "swiper/css/navigation";
import { Swiper, SwiperClass, SwiperProps, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Autoplay } from "swiper";
import React, { useMemo, useState } from "react";
import { ChevronRightIcon, ChevronLeftIcon } from "@heroicons/react/24/outline";

interface SwiperSlidesProps extends SwiperProps {
  slides: Array<React.ReactNode>;
  slidesPerView?: number;
}

const SwiperSlides: FC<SwiperSlidesProps> = ({
  slides,
  slidesPerView = 1,
  ...props
}) => {
  const [swiperRef, setSwiperRef] = useState<SwiperClass>();
  const next = () => {
    swiperRef && swiperRef.slideNext();
  };

  const breakpoints = {
    0: { slidesPerView: 1, spaceBetween: props.spaceBetween },
    640: {
      slidesPerView: slidesPerView !== 1 ? 2 : 1,
      spaceBetween: props.spaceBetween,
    },
    768: {
      slidesPerView: slidesPerView !== 1 ? 3 : 1,
      spaceBetween: props.spaceBetween,
    },
    1024: {
      slidesPerView: slidesPerView,
      spaceBetween: props.spaceBetween,
    },
    1280: {
      slidesPerView: slidesPerView,
      spaceBetween: props.spaceBetween,
    },
    1536: {
      slidesPerView: slidesPerView,
      spaceBetween: props.spaceBetween,
    },
  };
  return (
    <div className="relative flex items-center gap-2">
      <div
        className="absolute z-10 left-0 p-1 bg-primary-600/50 text-white"
        onClick={() => swiperRef && swiperRef.slidePrev()}
      >
        <ChevronLeftIcon className="h-4 w-4" />
      </div>
      <Swiper
        modules={[Navigation, Pagination, Autoplay]}
        onSwiper={(swiper) => {
          setSwiperRef(swiper);
        }}
        breakpoints={
          slidesPerView > 1 && slidesPerView < slides.length
            ? breakpoints
            : undefined
        }
        {...props}
      >
        {slides.map((slide, index) => (
          <SwiperSlide key={`slide-#${index}`}>{slide}</SwiperSlide>
        ))}
      </Swiper>
      <div
        className="absolute z-10 right-0 p-1 bg-primary-600/50 text-white"
        onClick={next}
      >
        <ChevronRightIcon className="h-4 w-4" />
      </div>
    </div>
  );
};
export default SwiperSlides;
