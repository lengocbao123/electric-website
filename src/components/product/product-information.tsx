import { Product } from "@/types";
import {
  ChatBubbleLeftIcon,
  EnvelopeIcon,
  PhoneIcon,
} from "@heroicons/react/24/outline";
import Image from "next/image";
import type { FC } from "react";

interface ProductInformationProps {
  product: Product;
}

const ProductInformation: FC<ProductInformationProps> = ({ product }) => {
  return (
    <div className="w-full grid grid-cols-1 sm:grid-cols-3 gap-5 px-4 sm:px-0">
      <Image
        alt={product.name}
        src={product.images[0]}
        className="object-cover w-full h-auto"
        width={400}
        height={300}
      />
      <div className="col-span-2 flex flex-col gap-5">
        <h1 className="text-lg font-bold text-neutral-800">{product.name}</h1>
        <div className="flex divide-x">
          <div className="flex gap-2 text-sm pr-2">
            <span>Mã SP:</span>
            <span>0001</span>
          </div>
          <div className="flex gap-2 text-sm px-2">
            <span>Danh mục:</span>
            <span>DANH MỤC DỰ ÁN</span>
          </div>
        </div>
        <h3 className="text-2xl text-error-500">Liên hệ</h3>
        <div className="flex flex-wrap gap-5">
          <a title="Liên hệ" href="#" className="p-2 bg-neutral-200 rounded-sm">
            <PhoneIcon className="text-neutral-600 h-4 w-4" />
          </a>
          <a title="Liên hệ" href="#" className="p-2 bg-neutral-200 rounded-sm">
            <EnvelopeIcon className="text-neutral-600 h-4 w-4" />
          </a>
          <a title="Liên hệ" href="#" className="p-2 bg-neutral-200 rounded-sm">
            <ChatBubbleLeftIcon className="text-neutral-600 h-4 w-4" />
          </a>
        </div>
      </div>
    </div>
  );
};
export default ProductInformation;
