import Image from "next/image";
import Link from "next/link";
import type { FC } from "react";

interface ProductCardProps {
  name: string;
  description: string;
  url: string;
  image: string;
}

const ProductCard: FC<ProductCardProps> = ({
  name,
  description,
  url,
  image,
}) => {
  return (
    <div className="flex flex-col gap-2 p-4 border rounded-sm">
      <div className="relative">
        <Image
          alt={name}
          src={image}
          height={165}
          width={220}
          className="object-cover w-full"
        />
      </div>
      <Link aria-label={name} title={name} href={url}>
        <h3 className="text-base text-primary-600 font-bold">{name}</h3>
      </Link>
      <p className="text-sm text-neutral-800 line-clamp-3">{description}</p>
    </div>
  );
};
export default ProductCard;
