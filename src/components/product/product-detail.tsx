import { Product } from "@/types";
import type { FC } from "react";

interface ProductDetailProps {
  product: Product;
}

const ProductDetail: FC<ProductDetailProps> = ({ product }) => {
  return (
    <div className="flex flex-col divide-y px-4 sm:px-0">
      <div className="text-base font-bold border-b-2 border-neutral-800 w-fit">
        Chi tiết
      </div>
      <div
        className="text-sm text-neutral-800 text-justify flex flex-col gap-3 py-4"
        dangerouslySetInnerHTML={{ __html:  product.detail }}
      ></div>
    </div>
  );
};
export default ProductDetail;
