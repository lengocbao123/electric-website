import { type FC } from "react";
import ProductCard from "@/components/product/product-card";
import { Product } from "@/types";
import { convertToSlug } from "@/utils/text";

interface ProductListProps {
  products: Array<Product>;
}

const ProductList: FC<ProductListProps> = ({ products }) => {
  return (
    <div className="flex flex-col gap-5">
      <div className="grid grid-cols-1 sm:grid-cols-4  gap-5">
        {products.map((product) => (
          <ProductCard
            key={product.id}
            name={product.name}
            image={product.thumbnail}
            description={product.description}
            url={`/san-pham/${convertToSlug(product.name)}/${product.id}`}
          />
        ))}
      </div>
    </div>
  );
};
export default ProductList;
