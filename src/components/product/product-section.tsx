import type { FC } from "react";
import Intro from "@/components/ui/intro";
import ProductList from "@/components/product/product-list";
import { Product } from "@/types";
import Link from "next/link";

interface ProductSectionProps {
  products: Array<Product>;
}

const ProductSection: FC<ProductSectionProps> = ({ products }) => (
  <div className="flex flex-col gap-20">
    <Intro
      title="Sản phẩm nổi bật"
      description="Vinacontrol là tổ chức đánh giá sự phù hợp được thành lập đầu tiên tại Việt Nam. 
      Được thành lập năm 1957, Vinacontrol cung cấp các giải pháp dịch vụ hỗ trợ doanh nghiệp đảm bảo chất lượng hàng hóa và giảm thiểu rủi ro ở hầu hết lĩnh vực hàng hóa trọng điểm của nền kinh tế."
    />
    <ProductList products={products} />
    <Link
      aria-label="Xem Thêm"
      title="Xem Thêm"
      href="/san-pham"
      className="px-4 py-2 rounded-full bg-transparent border border-primary-600 mx-auto"
    >
      Xem Thêm
    </Link>
  </div>
);
export default ProductSection;
