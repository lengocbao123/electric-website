import { type FC } from "react";
import ServiceCard from "@/components/service/service-card";
import SwiperSlides from "@/components/ui/swiper";

type ServiceSlide = {
  name: string;
  description: string;
  url: string;
  image: string;
};
interface ServiceSlidesProps {
  slides: Array<ServiceSlide>;
}

const ServiceSlides: FC<ServiceSlidesProps> = ({ slides }) => {
  return (
    <div>
      <SwiperSlides
        spaceBetween={30}
        slidesPerView={4}
        slides={slides.map((slide) => {
          return (
            <div key={slide.name}>
              <ServiceCard {...slide} />
            </div>
          );
        })}
      />
    </div>
  );
};
export default ServiceSlides;
