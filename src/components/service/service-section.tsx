import type { FC } from "react";
import Intro from "@/components/ui/intro";
import ServiceSlides from "@/components/service/service-slides";

interface ServiceSectionProps {}
const SERVICE_SLIDES = [
  {
    name: "Giám định",
    description:
      "Giám định là hoạt động nghiệp vụ kỹ thuật để xem xét sự phù hợp của sản phẩm, hàng hóa so với hợp đồng hoặc tiêu chuẩn công bố áp dụng, quy chuẩn kỹ thuật tương ứng.",
    url: "#",
    image: "/images/service-1.png",
  },
  {
    name: "Thử nghiệm",
    description:
      "Giám định là hoạt động nghiệp vụ kỹ thuật để xem xét sự phù hợp của sản phẩm, hàng hóa so với hợp đồng hoặc tiêu chuẩn công bố áp dụng, quy chuẩn kỹ thuật tương ứng.",
    url: "#",
    image: "/images/service-1.png",
  },
  {
    name: "Chứng nhận",
    description:
      "Giám định là hoạt động nghiệp vụ kỹ thuật để xem xét sự phù hợp của sản phẩm, hàng hóa so với hợp đồng hoặc tiêu chuẩn công bố áp dụng, quy chuẩn kỹ thuật tương ứng.",
    url: "#",
    image: "/images/service-1.png",
  },
  {
    name: "Kiểm định & Hiệu chuẩn",
    description:
      "Giám định là hoạt động nghiệp vụ kỹ thuật để xem xét sự phù hợp của sản phẩm, hàng hóa so với hợp đồng hoặc tiêu chuẩn công bố áp dụng, quy chuẩn kỹ thuật tương ứng.",
    url: "#",
    image: "/images/service-1.png",
  },
  {
    name: "Phục vụ Quản Lý Nhà nước",
    description:
      "Giám định là hoạt động nghiệp vụ kỹ thuật để xem xét sự phù hợp của sản phẩm, hàng hóa so với hợp đồng hoặc tiêu chuẩn công bố áp dụng, quy chuẩn kỹ thuật tương ứng.",
    url: "#",
    image: "/images/service-1.png",
  },
];

const ServiceSection: FC<ServiceSectionProps> = ({}) => (
  <div className="flex flex-col gap-20">
    <Intro
      title="Dịch vụ"
      description="Vinacontrol là đơn vị có uy tín hàng đầu trong lĩnh vực tư vấn và cung cấp dịch vụ đánh giá sự phù hợp. Các chuyên gia của chúng tôi sẵn sàng đưa ra các giải pháp hỗ trợ bạn đảm bảo chất lượng và sự an toàn của hàng hóa ngay từ khâu nguyên liệu đầu vào, trong quá trình sản xuất, vận chuyển, giao thương và đến tay người tiêu dùng."
    />
    <ServiceSlides slides={SERVICE_SLIDES} />
  </div>
);
export default ServiceSection;
