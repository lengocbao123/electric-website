import Image from "next/image";
import Link from "next/link";
import type { FC } from "react";

interface ServiceCardProps {
  name: string;
  description: string;
  url: string;
  image: string;
}

const ServiceCard: FC<ServiceCardProps> = ({
  name,
  description,
  url,
  image,
}) => {
  return (
    <div className="flex flex-col gap-2 p-4 border rounded-sm">
      <div className="relative">
        <Image
          alt={name}
          src={image}
          height={165}
          width={220}
          className="object-cover w-full"
        />
      </div>
      <Link aria-label={name} title={name} href={url}>
        <h3 className="text-base text-primary-600 font-bold line-clamp-1"> {name}</h3>
      </Link>
      <div className="h-1 w-10 bg-primary-600"></div>
      <p className="text-sm line-clamp-3">{description}</p>
    </div>
  );
};
export default ServiceCard;
