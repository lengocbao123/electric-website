import { useState, type FC } from "react";
import { Transition } from "@headlessui/react";
import { Bars3Icon } from "@heroicons/react/24/outline";
import Image from "next/image";
import { HeaderMenuProps } from "@/components/menu/header-menu";
import Link from "next/link";
import clsx from "clsx";
import { COMPANY_NAME } from "@/constants/configuration";

const MobileHeaderMenu: FC<HeaderMenuProps> = ({ items }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="w-full flex sm:hidden flex-col bg-white px-4 border-2 border-b-primary-600">
      <div
        className={clsx(
          "w-full py-7.5  flex items-center justify-between",
          isOpen && "border-b"
        )}
      >
        <Link aria-label={COMPANY_NAME} title={COMPANY_NAME} href="/">
          <Image
            className="mt-5"
            alt="logo"
            src="/images/logo.png"
            width={250}
            height={53}
          />
        </Link>
        <button
          aria-label="menu-btn"
          className="bg-transparent rounded-lg border p-2"
          onClick={toggleMenu}
        >
          <Bars3Icon className="h-6 w-6" />
        </button>
      </div>

      <Transition
        show={isOpen}
        enter="transition-opacity duration-75"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition-opacity duration-150"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        {items.map((item) => (
          <div key={item.id} className="py-4">
            <Link
              title={item.text}
              className="font-bold text-base"
              href={item.href}
            >
              {item.text}
            </Link>
          </div>
        ))}
      </Transition>
    </div>
  );
};
export default MobileHeaderMenu;
