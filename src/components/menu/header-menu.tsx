import type { FC } from "react";
import Image from "next/image";
import Link from "next/link";
import { COMPANY_NAME } from "@/constants/configuration";
export type MenuItem = {
  text: string;
  href: string;
  id: string;
};
export interface HeaderMenuProps {
  items: Array<MenuItem>;
}

const HeaderMenu: FC<HeaderMenuProps> = ({ items }) => {
  return (
    <div className="sticky top-0 z-20 hidden sm:flex flex-wrap items-start w-full bg-white px-4 border-2 border-b-primary-600">
      <Link aria-label={COMPANY_NAME} title={COMPANY_NAME} href="/">
        <Image
          className="mt-5"
          alt="logo"
          src="/images/logo.png"
          width={250}
          height={53}
        />
      </Link>

      <div className="flex-grow my-7.5 flex justify-start lg:justify-end gap-10 items-center">
        {items.map((item) => (
          <div
            key={item.id}
            className={
              "pb-4 first-line:flex flex-col border-b-[20px] border-transparent hover:border-primary-600"
            }
          >
            <Link
              title={item.text}
              className="font-bold text-base"
              href={item.href}
            >
              {item.text}
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};
export default HeaderMenu;
