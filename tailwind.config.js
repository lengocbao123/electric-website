const plugin = require("tailwindcss/plugin");
const { fontFamily } = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: "#2d7d4e",
          50: "#f0f9f3",
          100: "#dbf0df",
          200: "#bae0c4",
          300: "#8cc9a0",
          400: "#5cab78",
          500: "#3a8f5b",
          600: "#2d7d4e",
          700: "#215b3b",
          800: "#1c4930",
          900: "#183c29",
        },
        secondary: {
          50: "#fff9eb",
          100: "#ffefc6",
          200: "#ffdc88",
          300: "#ffbf3c",
          400: "#ffac20",
          500: "#f98807",
          600: "#dd6202",
          700: "#b74206",
          800: "#94320c",
          900: "#7a2b0d",
        },
        neutral: {
          DEFAULT: "#ffffff",
          50: "#f5f5f6",
          100: "#f5f5f8",
          200: "#edeef2",
          300: "#dedfe7",
          400: "#bbbbcc",
          500: "#8c8c99",
          600: "#666874",
          700: "#4a4b54",
          800: "#35353a",
          900: "#3a3b3f",
        },
        info: {
          50: "#eef6ff",
          100: "#daebff",
          200: "#bdddff",
          300: "#90c8ff",
          400: "#59a8ff",
          500: "#3587fc",
          600: "#1f67f1",
          700: "#1751de",
          800: "#1942b4",
          900: "#1a3b8e",
        },
        success: {
          50: "#eeffef",
          100: "#d7ffdd",
          200: "#b2ffbd",
          300: "#76ff8b",
          400: "#33f552",
          500: "#09de2b",
          600: "#00c01f",
          700: "#04911c",
          800: "#0a711c",
          900: "#0a5d1a",
        },
        error: {
          50: "#fff1f0",
          100: "#ffe1de",
          200: "#ffc7c2",
          300: "#ffa098",
          400: "#ff695c",
          500: "#ff3b2a",
          600: "#ef1907",
          700: "#d11404",
          800: "#ac1508",
          900: "#8e180e",
        },
      },
      spacing: {
        0.75: "0.1875rem", // 3px
        1.25: "0.3125rem", // 5px
        1.75: "0.4375rem", // 7px
        2.25: "0.5625rem", // 9px
        2.5: "0.625rem", // 10px
        2.75: "0.6875rem", // 11px
        3.25: "0.8125rem", // 13px
        4.25: "1.0625rem", // 17px
        4.5: "1.125rem", // 18px
        5.25: "1.3125rem", // 21px
        5.5: "1.375rem", // 22px
        6.25: "1.5625rem", // 25px
        7.5: "1.875rem", // 30px
        15: "3.75rem", // 60px
        16.5: "4.125rem", // 66px
        18: "4.5rem", // 72px
        30: "7.5rem", // 120px
        33: "8.25rem", // 132px
      },
    },
  },
  future: {
    hoverOnlyWhenSupported: true,
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/line-clamp"),
  ],
};
