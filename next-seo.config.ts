export const title = "CÔNG TY TNHH THƯƠNG MẠI VÀ XÂY LẮP TRUNG NAM";
export const description =
	"xây lắp trạm điện, hạ trạm biến áp, lắp đặt trạm biến áp, tủ điện trung thế, tủ điện hạ thế, xây lắp điện, sửa chữa bảo trì thí nghiệm";
export const url = "https://electric-website.vercel.app";
export const keywords =
	"xây lắp trạm điện, hạ trạm biến áp, lắp đặt trạm biến áp, tủ điện trung thế, tủ điện hạ thế, xây lắp điện, sửa chữa bảo trì thí nghiệm";

/** @type {import('next-seo').DefaultSeoProps} */
const nextSeoConfig = {
	title,
	titleTemplate: "%s | TRUNG NAM",
	description,
	canonical: url,
	image:"",
	openGraph: {
		url,
		type: "website",
		title,
		description,
		locale: "vi_VN",
	},
};

export default nextSeoConfig;
